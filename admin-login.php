<?php
/*
 * @package admin-login
 */
/*
Plugin Name: Admin Login
Plugin URI: https://dinesh-ghimire.com.np/
Description: This plugin for Choose admin login template
Version: 1.0
Author: Dinesh Ghimire
Author URI: https://dinesh-ghimire.com.np
License: GPLv2 or later
Text Domain: admin-login
*/

if( !defined('ABSPATH') ){

	exit;

}
require_once('login-option-template.php');
