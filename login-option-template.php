<?php

function registerOptionGroup(){

    register_setting( 'choose_login_template_option_group', 'choose_login_template_option_group_key' );

    register_setting( 'choose_login_template_option_group', 'choose_login_template_data' );

}

function loginTemplate(){

    ?>

    <h2>Choose Login Template</h2>

    <form method="post" action="options.php">

        <?php settings_fields( 'choose_login_template_option_group' ); ?>

        <?php do_settings_sections( 'choose_login_template_option_group' ); ?>

        <table class="form-table api_master_admin">

            <tr class="login_template_row" valign="top">
                <th scope="row">Choose Login Template</th>
                <td>
                    <select name="choose_login_template_option_group_key" id="choose_login_template_option_group_key">
                        <?php
                        $select_option = get_option('choose_login_template_option_group_key');
                        $loginTemplateData = get_option('choose_login_template_data');

                        print_r($loginTemplateData);

                        $templateList = array(
                            'Default',
                            'Template 1',
                            'Template 2',
                            'Template 3',
                            'Template 4',
                            'Template 5',
                        );

                        foreach($templateList as $key=>$value){
                            $selected = ($key==$select_option) ? 'selected="selected"' : '';
                            echo '<option value="'. $key .'"'.$selected.'>'.$value.'</option>';
                        }

                        ?>
                    </select>
                </td>
            </tr>

            <tr class="login_template_row" valign="top">
                <th>Login Form Width</th>
                <td>
                    <input  type="text" name="choose_login_template_data['max_width']" value="<?php echo ( isset( $loginTemplateData['max_width'] ) ) ? $loginTemplateData['max_width'] : ''; ?>" />
                </td>
            </tr>

            <tr class="login_template_row" valign="top">
                <th>Login Form Buttton Color</th>
                <td>
                    <input  type="text" name="choose_login_template_data['button_color']" value="<?php echo ( isset( $loginTemplateData['button_color'] ) ) ? $loginTemplateData['button_color'] : ''; ?>" />
                </td>
            </tr>

            <tr class="login_template_row" valign="top">
                <th>Login Form Background Color</th>
                <td>
                    <input  type="text" name="choose_login_template_data['background_color']" value="<?php echo ( isset( $loginTemplateData['background_color'] ) ) ? $loginTemplateData['background_color'] : ''; ?>" />
                </td>
            </tr>

            <tr class="login_template_row" valign="top">

                <td></td>
                <td>
                    <input  type="submit" name="submit" value="submit" />
                </td>

            </tr>

        </table>

    </form>

    <?php

}

function addOptionMenu(){

    add_menu_page("Login Template", "Login Template", "manage_options", "login-template", 'loginTemplate', null, 60);

    add_action( 'admin_init', 'registerOptionGroup' );

}

add_action("admin_menu", 'addOptionMenu' );


function addStyleLoginPage() {

    $select_option = get_option('choose_login_template_option_group_key');

    wp_enqueue_style( 'login-styles', plugins_url( 'assets/css/style'. $select_option .'.css', __FILE__ ), false );

}

function addScriptLoginPage() {

    $select_option = get_option('choose_login_template_option_group_key');

    wp_enqueue_script( 'jQuery', 'https://code.jquery.com/jquery-1.10.0.min.js', false );
    wp_enqueue_script( 'login-js', plugins_url( 'assets/js/custom'. $select_option .'.js', __FILE__ ), false );

}



function custom_login_deregister(){



    wp_deregister_style( 'login' );

    add_action( 'login_enqueue_scripts', 'addStyleLoginPage', 10 );

    add_action( 'login_enqueue_scripts', 'addScriptLoginPage', 1 );

}



function chooseTemplate(){

    $select_option = get_option('choose_login_template_option_group_key');

    if($select_option && $GLOBALS['pagenow'] === 'wp-login.php'){

        add_action( 'login_init', 'custom_login_deregister' );

        require_once('templates/template'.$select_option.'.php');

    }

}

chooseTemplate();